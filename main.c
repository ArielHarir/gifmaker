#define _CRT_SECURE_NO_WARNINGS                   //Final project - Ariel Harir
#define CV_IGNORE_DEBUG_BUILD_GUARD
#define STR_LEN 50

#include <stdio.h>
#include <Windows.h>
#include <string.h>
#include <opencv2/imgcodecs/imgcodecs_c.h>
#include <opencv2/core/core_c.h>
#include <opencv2/highgui/highgui_c.h>
#include "linkedList.h"
#include "helper.h"
#include "view.h"
#include "saveAndLoad.h"


int main(void)
{
	int choice1 = 0, choice2 = -1, duration = 0, num = 0, ret = 0;
	char path[STR_LEN] = { 0 };
	char name[STR_LEN] = { 0 };
	FrameNode* head = NULL;
	FrameNode* newFrame = NULL;
	FILE* file = NULL;

	choice1 = printAndGetNum("Welcome to Magshimim Movie Maker! what would you like to do?\n [0] Create a new project\n [1] Load existing project\n");
	choice1 = checkValidInput(choice1);

	if (choice1 == 0)
	{
		printf("Working on a new project.\n");
	}
	else
	{
		printf("Enter the path of the project (including project name):\n");
		myFgets(path, STR_LEN);
		file = fopen(path, "r");
		if (checkValidPath(path) == 1)
		{
			printf("Error!- cant open file, creating a new project");
		}
		else
		{
			loadFile(file, &head, newFrame, name, duration);
		}
	}

	while (choice2 != 0)
	{
		choice2 = printAndGetNum("\nWhat would you like to do?\n [0] Exit\n [1] Add new Frame\n [2] Remove a Frame\n [3] Change frame index\n [4] Change frame duration\n [5] Change duration of all frames\n [6] List frames\n [7] Play movie!\n [8] Save project\n");
		choice2 = checkValidInputMenu(choice2);
		switch (choice2)
		{
		case 0:
			printf("\nBye!");
			break;
		case 1:
			printf("***Creating new frame ***\nPlease insert frame path:\n");
			myFgets(path, STR_LEN);
			duration = printAndGetNum("Please insert frame duration(in miliseconds):\n");
			duration = checkValidDuration(duration);
			printf("Please choose a name for that frame:\n");
			myFgets(name, STR_LEN);
			while (findFrame(&head, name) == 1)
			{
				printf("The name is already taken, please enter another name\n");
				myFgets(name, STR_LEN);
			}
			if (checkValidPath(path) == 0)
			{
				newFrame = createFrameNode(name, duration, path);
				addNewFrame(&head, newFrame);
			}
			break;
		case 2:
			printf("Enter the name of the frame you wish to erase\n");
			myFgets(name, STR_LEN);
			if (findFrame(&head, name) == 1)
			{
				removeFrame(&head, name);
			}
			else
			{
				printf("The frame was not found\n");
			}
			break;
		case 3:
			printf("Enter the name of the frame\n");
			myFgets(name, STR_LEN);
			if (findFrame(&head, name) == 1)
			{
				num = printAndGetNum("Enter the new index in the movie you wish to place the frame\n");
				ret = listLength(head);
				while (num > ret)
				{
					printf("The movie contains only % d frames!\n", ret);
					num = printAndGetNum("Enter the new index in the movie you wish to place the frame\n");
				}
				changePlace(&head, name, num);
			}
			else
			{
				printf("this frame does not exist\n");
			}
			break;
		case 4:
			printf("enter the name of the frame\n");
			myFgets(name, STR_LEN);
			if (findFrame(&head, name) == 0)
			{
				printf("The frame does not exist\n");
			}
			else
			{
				num = printAndGetNum("Enter the new duration\n");
				changeFrameDuration(&head, name, num);
			}
			break;
		case 5:
			num = printAndGetNum("Enter the duration for all frames:\n");
			changeDurationAllFrames(&head, num);
			break;
		case 6:
			printList(head);
			break;
		case 7:
			play(head);
			break;
		case 8:
			printf("Where to save the project? enter a full path and file name\n");
			myFgets(path, STR_LEN);
			saveList(&head, path, file);
			break;
		}
	}
	freeList(&head);
	getchar();
	return 0;
}
