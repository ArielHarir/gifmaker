#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <Windows.h>
#include <stdlib.h>
#include <string.h>
#include "linkedList.h"

/*
The function creates node
input: the name of the frame, the duration and the path to the picture.
output: pointr to the new node.
*/
FrameNode* createFrameNode(char* name, unsigned int duration, char* path)
{
	FrameNode* newFrame = (FrameNode*)malloc(sizeof(FrameNode));
	if (newFrame)
	{

		Frame* f = ((Frame*)malloc(sizeof(Frame)));
		f->duration = duration;
		f->name = malloc(sizeof(char) * strlen(name) + 1);
		strncpy(f->name, name, strlen(name) + 1);
		f->path = malloc(sizeof(char) * strlen(path) + 1);
		strncpy(f->path, path, strlen(path) + 1);

		newFrame->frame = f;
		newFrame->next = NULL;
	}

	return newFrame;
}

/*
The function add new frame to the list.
input: pointer to the first frame and pointer to new frame.
output: none
*/
void addNewFrame(FrameNode** firstFrame, FrameNode* newFrame)
{
	if (!*firstFrame)
	{
		*firstFrame = newFrame;
	}
	else
	{
		FrameNode* p = *firstFrame;
		while (p->next)
		{
			p = p->next;
		}
		p->next = newFrame;
	}
}

/*
The function removes frame from the list.
input: the first node in the list, name of frame to remove.
output: 1 if frame has removed or 0 if it not found.
*/
void removeFrame(FrameNode** firstFrame, char* name)
{
	FrameNode* p = *firstFrame;
	FrameNode* dNode = NULL;
	if (*firstFrame)
	{
		if (findFrame(firstFrame, name) == 1)
		{
			*firstFrame = (*firstFrame)->next;
			free(p->frame->name);
			free(p->frame->path);
			free(p->frame);
			free(p);
		}
		else
		{
			while (p->next && 0 != findFrame(p->next, name))
			{
				p = p->next;
			}
			if (p->next)
			{
				dNode = p->next;
				p->next = dNode->next;
				free(dNode->frame->name);
				free(dNode->frame->path);
				free(dNode->frame);
				free(dNode);
			}
		}
	}
}

/*
The function search frames in the linked list by name.
input: pointer to the linked list, the name of the frame to search.
output: 1 if it found or 0 if not.
*/
int findFrame(FrameNode** firstFrame, char* name)
{
	int ret = 0;
	FrameNode* curr = *firstFrame;
	while (curr)
	{
		if (strcmp(curr->frame->name, name) == 0)
		{
			ret = 1;
			break;
		}
		curr = curr->next;
	}
	return ret;
}

/*
The function prints the list of the frames.
input: pointer to the linked list.
output: none
*/
void printList(FrameNode* firstFrame)
{
	printf("                Name            Duration        Path\n");
	FrameNode* curr = firstFrame;
	while (curr)
	{
		printf("		%s		%d ms		%s\n", curr->frame->name, curr->frame->duration, curr->frame->path);
		curr = curr->next;
	}
}

/*
The function finds the length of the list.
input: the first node in the list
output: return the length of the list.
*/
int listLength(FrameNode* firstFrame)
{
	int ans = 0;
	if (firstFrame != NULL)
	{
		ans = 1 + listLength(firstFrame->next);
	}
	return ans;
}

/*
The function free the linked list.
input:pointer to the linked list
output:none
*/
void freeList(FrameNode** firstFrame)
{
	int i = 0;
	if (*firstFrame != NULL)
	{
		FrameNode* curr = *firstFrame;
		FrameNode* temp = NULL;
		while (curr)
		{
			temp = curr->next;
			free(curr->frame->name);
			free(curr->frame->path);
			free(curr->frame);
			free(curr);
			curr = temp;
		}
		*firstFrame = NULL;
	}
}
/*
The function changes the duration of specific frame.
input: pointer to lists of frames, name of frame, new duration.
output: none.
*/
void changeFrameDuration(FrameNode** firstFrame, char* name, int num)
{
	FrameNode* curr = *firstFrame;
	while (curr)
	{
		if (strcmp(curr->frame->name, name) == 0)
		{
			curr->frame->duration = num;
			break;
		}
		curr = curr->next;
	}
}
/*
The function changes the duration of all the frames.
input: pointer to lists of frames, new duration.
output: none.
*/
void changeDurationAllFrames(FrameNode** firstFrame, int num)
{
	FrameNode* curr = *firstFrame;
	while (curr)
	{
		curr->frame->duration = num;
		curr = curr->next;
	}
}

/*
The function changes place of spcific frame in the list.
input: pointer to the linked list, name of frame in the list, the number of the new index.
This is Ariel Harir project - hope you did not copy the middle project from me.
output: none
*/
void changePlace(FrameNode** firstFrame, char* name, int num)
{
	int count = 1;
	FrameNode* tempX = NULL;
	FrameNode* tempY = NULL;
	FrameNode* currX = *firstFrame;
	FrameNode* currY = *firstFrame;
	while (currX && strcmp(currX->frame->name, name) == 0)
	{
		tempX = currX;
		currX = currX->next;
	}

	while (currY)
	{
		tempY = currY;
		currY = currY->next;
		if (count == num)
		{
			break;
		}
		count++;
	}

	if (currX == NULL || currY == NULL)
	{
		return;
	}
	if (tempX != NULL)
	{
		tempX->next = currY;
	}
	else
	{
		*firstFrame = currY;
	}
	if (tempY != NULL)
	{
		tempY->next = currX;
	}
	else
	{
		*firstFrame = currX;
	}

	FrameNode* temp = currY->next;
	currY->next = currX->next;
	currX->next = temp;
}
