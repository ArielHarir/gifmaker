#pragma once
#ifndef SAVEH
#define SAVEH
#include "linkedList.h"
#include <stdio.h>
#include <string.h>
void saveList(FrameNode** list, char* path, FILE* file);
void loadFile(FILE* f, FrameNode** head, FrameNode* newFrame, char* name, int duration);
void SaveAsGif(FrameNode** List, FILE* file, char* path);
#endif

