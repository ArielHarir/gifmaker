/*********************************
* Class: MAGSHIMIM Final Project *
* Play function				 	 *
**********************************/
#define _CRT_SECURE_NO_WARNINGS  
#include "view.h"

/**
play the movie!!
display the images each for the duration of the frame one by one and close the window
input: a linked list of frames to display
output: none
**/
void play(FrameNode* list)
{
	cvNamedWindow("Display window", CV_WINDOW_AUTOSIZE); //create a window
	FrameNode* head = list;
	int imgNum = 1, playCount = 0;
	IplImage* image;
	int num = 0, stop = 0;
	printf("if you want Endless loop enter [0] else enter [1]: ");
	scanf("%d", &num);
	if (num != 0 && num != 1)
	{
		printf("Invalid Input. if you want Endless loop enter [0] else enter [1]: ");
		scanf("%d", &num);
	}
	while (playCount < GIF_REPEAT)
	{
		while (list != 0)
		{
			image = cvLoadImage(list->frame->path, 1);
			IplImage* pGrayImg = 0;
			pGrayImg = cvCreateImage(cvSize(image->width, image->height), image->depth, 1);
			if (!image) //The image is empty - shouldn't happen since we checked already.
			{
				printf("Could not open or find image number %d", imgNum);
			}
			else
			{
				cvShowImage("Display window", image); //display the image
				cvWaitKey(list->frame->duration); //wait
				list = list->next;
				cvReleaseImage(&image);
			}
			imgNum++;
		}
		list = head; // rewind
		if (num == 0)
		{
			playCount--;
			if (playCount == -10 || playCount == -20)
			{
				printf("If you want to stop the loop enter 1 else enter any other number: \n");
				scanf("%d", &stop);
				if (stop == 1)
				{
					break;
				}
			}
		}
		else if (num == 1)
		{
			playCount++;
		}
	}
	cvDestroyWindow("Display window");
	return;
}
