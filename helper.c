#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <Windows.h>
#include "helper.h"


/*
The function check if the menu choice of the user is valid.
input: The choice of the user.
output: The valid choice.
*/
int checkValidInputMenu(int choice)
{
	while (choice > 8 || choice < 0)
	{
		printf("You should type one of the options - 0-8!\n\n");
		choice = printAndGetNum("What would you like to do?\n [0] Exit\n [1] Add new Frame\n [2] Remove a Frame\n [3] Change frame index\n [4] Change frame duration\n [5] Change duration of all frames\n [6] List frames\n [7] Play movie!\n [8] Save project\n");
	}
	return choice;
}
/*
The function check if the choice of the user is valid.
input: The choice of the user.
output: The valid choice.
*/
int checkValidInput(int choice)
{
	while (choice < 0 || choice > 1)
	{
		choice = printAndGetNum("Invalid choice, try again:\n[0] Create a new project\n[1] Load existing project\n");
	}
	return choice;
}
/*
The function prints a string and gets number from the user.
input: chars array to print
output: number
*/
int printAndGetNum(char* str)
{
	int num = 0;
	printf("%s", str);
	scanf("%d", &num);
	getchar();
	return num;
}

/*
Function will perform the fgets command and also remove the newline
that might be at the end of the string - a known issue with fgets.
input: the buffer to read into, the number of chars to read
output: none
*/
void myFgets(char str[], int n)
{
	fgets(str, n, stdin);
	str[strcspn(str, "\n")] = 0;
}

/*
The function checks if the path is valid.
input: chars array of the path.
output: 0 if it valid or 1 if not.
*/
int checkValidPath(char* path)
{
	int ret = 0;
	if (fopen(path, "r") == NULL)
	{
		printf("Can't find file! Frame will not be added\n");
		ret = 1;
	}
	return ret;
}

int checkValidDuration(int num)
{
	while (num < 0)
	{
		num = printAndGetNum("Invalid choice, try again:\nPlease insert frame duration(in miliseconds):\n");
	}
	return num;
}