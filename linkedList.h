#ifndef LINKEDLISTH
#define LINKEDLISTH
#define FALSE 0
#define TRUE !FALSE

// Frame struct
typedef struct Frame
{
	char* name;
	unsigned int duration;
	char* path;
} Frame;

// Link (node) struct
typedef struct FrameNode
{
	Frame* frame;
	struct FrameNode* next;
} FrameNode;

void addNewFrame(FrameNode** firstFrame, FrameNode* newFrame);
FrameNode* createFrameNode(char* name, unsigned int duration, char* path);
void removeFrame(FrameNode** firstFrame, char* name);
int findFrame(FrameNode** firstFrame, char* name);
void changePlace(FrameNode** firstFrame, char* name, int num);
void changeFrameDuration(FrameNode** firstFrame, char* name, int num);
void changeDurationAllFrames(FrameNode** firstFrame, int num);
void printList(FrameNode* FirstFrame);
void freeList(FrameNode** firstFrame);
int listLength(FrameNode* firstFrame);

#endif
