#define _CRT_SECURE_NO_WARNINGS
#define STR_LEN 100
#include "saveAndLoad.h"

/*
The function save The linked List in File.
input: pointer to linked list, string array - path, pointer to file.
output: none
*/
void saveList(FrameNode** list, char* path, FILE* f)
{
	f = fopen(path, "w");
	if (f == NULL)
	{
		printf("File open error\n");
	}
	else
	{
		FrameNode* curr = *list;
		while (curr)
		{
			fprintf(f, "%d,%s,%s\n", curr->frame->duration, curr->frame->name, curr->frame->path);
			curr = curr->next;
		}
	}
	fclose(f);
}

/*
The function load the file into linked list.
input: ptr to file, linked list and frame, the name of the frame, the duration and the path to the picture.
output: none.
*/
void loadFile(FILE* f, FrameNode** head, FrameNode* newFrame, char* name, int duration)
{
	int count = 1, num = 0;
	char line[STR_LEN] = { 0 };
	char* token = NULL;
	char comma[] = ",";
	char imgPath[STR_LEN] = { 0 };
	while (fgets(line, sizeof line, f) != NULL)
	{
		token = strtok(line, comma);
		while (token != NULL)
		{
			if (count == 1)
			{
				sscanf(token, "%d", &duration);
			}
			if (count == 2)
			{
				strncpy(name, token, sizeof(name));
			}
			if (count == 3)
			{
				strncpy(imgPath, token, sizeof(imgPath));
				imgPath[strcspn(imgPath, "\n")] = 0;
			}
			token = strtok(NULL, comma);
			count++;
		}
		newFrame = createFrameNode(name, duration, imgPath);
		addNewFrame(head, newFrame);
		count = 1;
	}
	fclose(f);
}

void SaveAsGif(FrameNode** List, FILE* file, char* path)
{

	if ((file = fopen(path, "wb")) == NULL)
	{
		printf("File open error\n ");
		return 1;
	}
	FrameNode* curr = *List;
	while (curr)
	{
		fwrite(curr->frame, sizeof(curr->frame), 1, file);
		curr = curr->next;
	}
	fclose(file);
}